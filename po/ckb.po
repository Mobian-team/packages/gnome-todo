# Kurdish (Sorani) translation for gnome-todo
# Copyright (c) 2020 Rosetta Contributors and Canonical Ltd 2020
# This file is distributed under the same license as the gnome-todo package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-todo\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2020-04-05 19:14+0000\n"
"PO-Revision-Date: 2020-05-04 18:00+0300\n"
"Last-Translator: Jwtiyar Nariman <jwtiyar@gmail.com>\n"
"Language-Team: Kurdish (Sorani) <ckb@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2020-05-04 14:52+0000\n"
"X-Generator: Poedit 2.3\n"
"Language: ckb\n"

#: data/appdata/org.gnome.Todo.appdata.xml.in:7
#: data/org.gnome.Todo.desktop.in:3 src/gtd-application.c:156
#: src/gtd-window.c:799 src/gtd-window.c:830 src/main.c:35
msgid "To Do"
msgstr "ئەرکەکان"

#: data/appdata/org.gnome.Todo.appdata.xml.in:8
msgid "Task manager for GNOME"
msgstr "ڕێکخەری کار بۆ گنۆم"

#: data/appdata/org.gnome.Todo.appdata.xml.in:11
msgid ""
"GNOME To Do is a task management application designed to integrate with "
"GNOME. It is extensible through plugins and supports custom task providers."
msgstr ""

#: data/appdata/org.gnome.Todo.appdata.xml.in:24
msgid "GNOME To Do with the dark theme variant"
msgstr ""

#: data/appdata/org.gnome.Todo.appdata.xml.in:28
msgid "Editing a tasklist with GNOME To Do"
msgstr ""

#: data/appdata/org.gnome.Todo.appdata.xml.in:32
msgid "Task lists displayed on grid mode"
msgstr ""

#: data/appdata/org.gnome.Todo.appdata.xml.in:36
msgid "Task lists displayed on list mode"
msgstr ""

#: data/appdata/org.gnome.Todo.appdata.xml.in:40
msgid "Available plugins for GNOME To Do"
msgstr ""

#: data/appdata/org.gnome.Todo.appdata.xml.in:44
msgid "Visualizing tasks for today on GNOME To Do’s Today panel"
msgstr ""

#: data/gtk/menus.ui:15 data/ui/plugin-dialog.ui:75
msgid "Extensions"
msgstr "زیادکراوەکان"

#: data/gtk/menus.ui:22
msgid "_About"
msgstr "_دەربارە"

#: data/gtk/menus.ui:26
msgid "_Quit"
msgstr "_دەرچوون"

#: data/org.gnome.Todo.desktop.in:4
msgid "Manage your personal tasks"
msgstr "ئەرکە تایبەتەکانت بەڕێوەببە"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: data/org.gnome.Todo.desktop.in:7
msgid "org.gnome.Todo"
msgstr "org.gnome.Todo"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Todo.desktop.in:13
msgid "Task;Productivity;Todo;"
msgstr "Task;Productivity;Todo;"

#: data/org.gnome.todo.gschema.xml:6
msgid "Window maximized"
msgstr "پەنجەرە گەورەکرایەوە"

#: data/org.gnome.todo.gschema.xml:7
msgid "Window maximized state"
msgstr "دۆخی پەنجەرەی گەورەکراوە"

#: data/org.gnome.todo.gschema.xml:11
msgid "Window size"
msgstr "قەبارەی پەنجەرە"

#: data/org.gnome.todo.gschema.xml:12
msgid "Window size (width and height)."
msgstr "قەبارەی پەنجەرە(پانی و بەرزی)"

#: data/org.gnome.todo.gschema.xml:16
msgid "Window position"
msgstr "شوێنی پەنجەرە"

#: data/org.gnome.todo.gschema.xml:17
msgid "Window position (x and y)."
msgstr "شوێنی پەنجەرە( x و y)"

#: data/org.gnome.todo.gschema.xml:21
msgid "First run of GNOME To Do"
msgstr "یەکەم کارکردنی GNOME To Do"

#: data/org.gnome.todo.gschema.xml:22
msgid ""
"Whether it’s the first run of GNOME To Do (to run the initial setup) or not"
msgstr ""

#: data/org.gnome.todo.gschema.xml:26
msgid "Default provider to add new lists to"
msgstr ""

#: data/org.gnome.todo.gschema.xml:27
msgid "The identifier of the default provider to add new lists to"
msgstr ""

#: data/org.gnome.todo.gschema.xml:31
msgid "List of active extensions"
msgstr "لیستی زیادکراوە چالاکەکان"

#: data/org.gnome.todo.gschema.xml:32
msgid "The list of active extensions"
msgstr "لیستی زیادکراوە چالاکەکان"

#: data/org.gnome.todo.gschema.xml:36
msgid "The current list selector"
msgstr "دیاریکەری لیستی ئێستا"

#: data/org.gnome.todo.gschema.xml:37
msgid "The current list selector. Can be “grid” or “list”."
msgstr ""

#: data/ui/edit-pane.ui:43
msgid "_Notes"
msgstr "_تێبینییەکان"

#: data/ui/edit-pane.ui:93
msgid "D_ue Date"
msgstr ""

#: data/ui/edit-pane.ui:111
msgid "_Today"
msgstr "_ئەمڕۆ"

#: data/ui/edit-pane.ui:121
msgid "To_morrow"
msgstr ""

#: data/ui/edit-pane.ui:169
msgid "_Priority"
msgstr "_پێشینە"

#: data/ui/edit-pane.ui:187
msgctxt "taskpriority"
msgid "None"
msgstr "هیج"

#: data/ui/edit-pane.ui:188
msgid "Low"
msgstr "نزم"

#: data/ui/edit-pane.ui:189
msgid "Medium"
msgstr "ناوەند"

#: data/ui/edit-pane.ui:190
msgid "High"
msgstr "بەرز"

#: data/ui/edit-pane.ui:198 data/ui/list-selector-panel.ui:93
msgid "_Delete"
msgstr "_سڕینەوە"

#: data/ui/edit-pane.ui:249
msgctxt "taskdate"
msgid "None"
msgstr "نییە"

#: data/ui/initial-setup.ui:25
msgid "Welcome"
msgstr "بەخێرهاتی"

#: data/ui/initial-setup.ui:40
msgid "Log in to online accounts to access your tasks"
msgstr "بچۆ هەژماری سەرهێڵ بۆ چوونەناوی ئەرکەکانت"

#: data/ui/initial-setup.ui:67
msgid "To Do Setup"
msgstr "ڕێکخستنی To Do"

#: data/ui/initial-setup.ui:70 data/ui/provider-popover.ui:43
#: data/ui/window.ui:74
msgid "_Cancel"
msgstr "_پاشگەزبوونەوە"

#: data/ui/initial-setup.ui:80
msgid "_Done"
msgstr "_ئەنجامدرا"

#: data/ui/list-selector-panel.ui:79 data/ui/list-selector-panel.ui:173
msgid "_Rename"
msgstr "_ناوگۆڕین"

#: data/ui/list-selector-panel.ui:119 src/views/gtd-list-selector-panel.c:469
msgid "Lists"
msgstr "لیستەکان"

#: data/ui/list-selector-panel.ui:129
msgid "Tasks"
msgstr "ئەرکەکان"

#: data/ui/list-selector-panel.ui:147
msgid "Name of the task list"
msgstr "ناوی لیستی ئەرکەکان"

#: data/ui/list-selector-panel.ui:263
msgid "_New List"
msgstr "_لیستی نوێ"

#: data/ui/list-view.ui:113
msgid "Show or hide completed tasks"
msgstr "ئەرکە تەواوکراوەکان پیشان بدە و بشارەوە"

#: data/ui/list-view.ui:140 src/gtd-task-list-view.c:388
msgid "Done"
msgstr "ئه‌نجام درا"

#: data/ui/new-task-row.ui:29
msgid "New task…"
msgstr "ئەرکی نوێ..."

#: data/ui/plugin-dialog.ui:118
msgid "No extensions found"
msgstr "هیچ پێوەکراوێک نیە"

#: data/ui/provider-popover.ui:25
msgid "Create _List"
msgstr "لیست _درووستبکە"

#: data/ui/provider-popover.ui:59
msgid "List Name"
msgstr "ناوی لیستە"

#: data/ui/provider-popover.ui:155
msgid "Select a storage location"
msgstr "شوێنی بیرگە هەڵبژێرە"

#: data/ui/provider-row.ui:70
msgid "Off"
msgstr "کوژاوەتەوە"

#: data/ui/provider-selector.ui:27
msgid "Click to add a new Google account"
msgstr "کرتە بکە بۆ زیادکردنی هەژماری گووگڵ"

#: data/ui/provider-selector.ui:51
msgid "Google"
msgstr "گووگڵ"

#: data/ui/provider-selector.ui:67
msgid "Click to add a new ownCloud account"
msgstr ""

#: data/ui/provider-selector.ui:91
msgid "ownCloud"
msgstr "ownCloud"

#: data/ui/provider-selector.ui:107
msgid "Click to add a new Microsoft Exchange account"
msgstr ""

#: data/ui/provider-selector.ui:131
msgid "Microsoft Exchange"
msgstr "Microsoft Exchange"

#: data/ui/provider-selector.ui:154
msgid "Or you can just store your tasks on this computer"
msgstr ""

#: plugins/background/gtd-plugin-background.c:150
#, c-format
msgid "%1$s and one more task"
msgid_plural "%1$s and %2$d other tasks"
msgstr[0] ""
msgstr[1] ""

#: plugins/background/gtd-plugin-background.c:249
#, c-format
msgid "You have %d task for today"
msgid_plural "You have %d tasks for today"
msgstr[0] ""
msgstr[1] ""

#: plugins/background/org.gnome.todo.background.gschema.xml:6
msgid "Run To Do on startup"
msgstr ""

#: plugins/background/org.gnome.todo.background.gschema.xml:7
msgid "Whether GNOME To Do should run on startup"
msgstr ""

#: plugins/background/org.gnome.todo.background.gschema.xml:11
msgid "Show notifications on startup"
msgstr ""

#: plugins/background/org.gnome.todo.background.gschema.xml:12
msgid "Whether GNOME To Do should show notifications or not"
msgstr ""

#: plugins/background/ui/preferences.ui:53
msgid "Run on Startup"
msgstr "لە سەرەتاوە دەستپێبکە"

#: plugins/background/ui/preferences.ui:66
msgid "Run To Do automatically when you log in"
msgstr ""

#: plugins/background/ui/preferences.ui:122
msgid "Show Notifications"
msgstr ""

#: plugins/background/ui/preferences.ui:135
msgid "When To Do runs, show a startup notification"
msgstr ""

#: plugins/eds/gtd-plugin-eds.c:201
msgid "Error loading GNOME Online Accounts"
msgstr ""

#: plugins/eds/gtd-provider-eds.c:139
msgid "Failed to connect to task list"
msgstr ""

#: plugins/eds/gtd-provider-eds.c:351
msgid "An error occurred while creating a task"
msgstr ""

#: plugins/eds/gtd-provider-eds.c:383
msgid "An error occurred while modifying a task"
msgstr ""

#: plugins/eds/gtd-provider-eds.c:404
msgid "An error occurred while removing a task"
msgstr ""

#: plugins/eds/gtd-provider-eds.c:422
msgid "An error occurred while creating a task list"
msgstr ""

#: plugins/eds/gtd-provider-eds.c:444 plugins/eds/gtd-provider-eds.c:467
msgid "An error occurred while modifying a task list"
msgstr ""

#: plugins/eds/gtd-provider-local.c:50
msgid "Local"
msgstr "ناوخۆیی"

#: plugins/eds/gtd-provider-local.c:56
msgid "On This Computer"
msgstr "لەسەر ئەم کۆمپیوتەرە"

#: plugins/eds/gtd-task-list-eds.c:455 plugins/eds/gtd-task-list-eds.c:482
#: plugins/eds/gtd-task-list-eds.c:506
msgid "Error fetching tasks from list"
msgstr ""

#: plugins/scheduled-panel/gtd-panel-scheduled.c:97 src/gtd-edit-pane.c:110
msgid "No date set"
msgstr "ڕێکەوت دیاریبکە"

#. Translators: This message will never be used with '1 day ago'
#. * but the singular form is required because some languages do not
#. * have plurals, some languages reuse the singular form for numbers
#. * like 21, 31, 41, etc.
#.
#: plugins/scheduled-panel/gtd-panel-scheduled.c:110
#, c-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "‫%d ڕۆژ پێش ئێستا"
msgstr[1] "‏‫%d ڕۆژ پێش ئێس"

#: plugins/scheduled-panel/gtd-panel-scheduled.c:114 src/gtd-task-row.c:147
msgid "Yesterday"
msgstr "دوێنێ"

#. Setup a title
#: plugins/scheduled-panel/gtd-panel-scheduled.c:118
#: plugins/score/score/__init__.py:101
#: plugins/today-panel/gtd-panel-today.c:134
#: plugins/today-panel/gtd-panel-today.c:139
#: plugins/today-panel/gtd-panel-today.c:273 src/gtd-task-row.c:139
msgid "Today"
msgstr "ئەمڕۆ"

#: plugins/scheduled-panel/gtd-panel-scheduled.c:122 src/gtd-task-row.c:143
msgid "Tomorrow"
msgstr "بەیانی"

#. Setup a title
#: plugins/scheduled-panel/gtd-panel-scheduled.c:356
#: plugins/scheduled-panel/gtd-panel-scheduled.c:358
#: plugins/scheduled-panel/gtd-panel-scheduled.c:495
msgid "Scheduled"
msgstr ""

#: plugins/scheduled-panel/gtd-panel-scheduled.c:500
#: plugins/today-panel/gtd-panel-today.c:278
#: plugins/unscheduled-panel/unscheduled-panel/__init__.py:58
msgid "Clear completed tasks…"
msgstr ""

#: plugins/score/score/__init__.py:90
msgid "No task completed today"
msgstr ""

#: plugins/todoist/gtd-provider-todoist.c:212
msgid "Error fetching Todoist account key"
msgstr ""

#: plugins/todoist/gtd-provider-todoist.c:213
msgid "Please ensure that Todoist account is correctly configured."
msgstr ""

#: plugins/todoist/gtd-provider-todoist.c:557
#, c-format
msgid ""
"GNOME To Do doesn’t have the necessary permissions to perform this action: %s"
msgstr ""

#: plugins/todoist/gtd-provider-todoist.c:565
#, c-format
msgid ""
"Invalid response received from Todoist servers. Please reload GNOME To Do."
msgstr ""

#: plugins/todoist/gtd-provider-todoist.c:865
msgid "An error occurred while updating a Todoist task"
msgstr ""

#: plugins/todoist/gtd-provider-todoist.c:982
msgid "An error occurred while retrieving Todoist data"
msgstr ""

#: plugins/todoist/gtd-provider-todoist.c:1057
msgid "An error occurred while updating Todoist"
msgstr ""

#: plugins/todoist/gtd-provider-todoist.c:1117
msgid "Todoist"
msgstr ""

#: plugins/todoist/gtd-provider-todoist.c:1426
#, c-format
msgid "Todoist: %s"
msgstr ""

#: plugins/todoist/ui/preferences.ui:81
msgid "No Todoist accounts found"
msgstr ""

#: plugins/todoist/ui/preferences.ui:90
msgid "Add a Todoist account"
msgstr ""

#: plugins/todo-txt/gtd-plugin-todo-txt.c:87
#: plugins/todo-txt/gtd-plugin-todo-txt.c:124
msgid "Cannot create Todo.txt file"
msgstr ""

#: plugins/todo-txt/gtd-plugin-todo-txt.c:161
msgid "Select a Todo.txt-formatted file:"
msgstr ""

#. Filechooser
#: plugins/todo-txt/gtd-plugin-todo-txt.c:165
msgid "Select a file"
msgstr "پەڕگە دیاریبکە"

#: plugins/todo-txt/gtd-plugin-todo-txt.c:191
msgid "Error opening Todo.txt file"
msgstr ""

#: plugins/todo-txt/gtd-plugin-todo-txt.c:201
msgid ""
"<b>Warning!</b> Todo.txt support is experimental and unstable. You may "
"experience instability, errors and eventually data loss. It is not "
"recommended to use Todo.txt integration on production systems."
msgstr ""

#: plugins/todo-txt/gtd-provider-todo-txt.c:326
msgid "Error while opening the file monitor. Todo.txt will not be monitored"
msgstr ""

#: plugins/todo-txt/gtd-provider-todo-txt.c:395
msgid "Todo.txt"
msgstr "‫Todo.txt"

#: plugins/todo-txt/gtd-provider-todo-txt.c:401
msgid "On the Todo.txt file"
msgstr ""

#: plugins/todo-txt/org.gnome.todo.txt.gschema.xml:6
msgid "Todo.txt File"
msgstr "پەڕگەی Todo.txt"

#: plugins/todo-txt/org.gnome.todo.txt.gschema.xml:7
msgid "Source of the Todo.txt file"
msgstr ""

#. Translators: 'Unscheduled' as in 'Unscheduled tasks'
#: plugins/unscheduled-panel/unscheduled-panel/__init__.py:41
#: plugins/unscheduled-panel/unscheduled-panel/__init__.py:103
msgid "Unscheduled"
msgstr ""

#. Translators: 'Unscheduled' as in 'Unscheduled tasks'
#: plugins/unscheduled-panel/unscheduled-panel/__init__.py:106
#, python-format
msgid "Unscheduled (%d)"
msgstr ""

#: src/gtd-application.c:72
msgid "Quit GNOME To Do"
msgstr ""

#: src/gtd-application.c:73
msgid "Enable debug messages"
msgstr ""

#: src/gtd-application.c:146
#, c-format
msgid "Copyright © %1$d The To Do authors"
msgstr ""

#: src/gtd-application.c:151
#, c-format
msgid "Copyright © %1$d–%2$d The To Do authors"
msgstr ""

#: src/gtd-application.c:163
msgid "translator-credits"
msgstr ""
"Launchpad Contributions:\n"
"  Jwtiyar Nariman https://launchpad.net/~jwtiyar"

#: src/gtd-empty-list-widget.c:51
msgid "No more tasks left"
msgstr "هیچ ئەرکێک نەمایەوە"

#: src/gtd-empty-list-widget.c:52
msgid "Nothing else to do here"
msgstr "هیچ نیە لێرە بیکەیت"

#: src/gtd-empty-list-widget.c:53
msgid "You made it!"
msgstr "ئەنجامت دا"

#: src/gtd-empty-list-widget.c:54
msgid "Looks like there’s nothing else left here"
msgstr ""

#: src/gtd-empty-list-widget.c:59
msgid "Get some rest now"
msgstr "هەندێک پشوو وەربگرە"

#: src/gtd-empty-list-widget.c:60
msgid "Enjoy the rest of your day"
msgstr "چێژ ببینە لە هەموو ڕۆژەکەت"

#: src/gtd-empty-list-widget.c:61
msgid "Good job!"
msgstr "کارێکی باشت کرد!"

#: src/gtd-empty-list-widget.c:62
msgid "Meanwhile, spread the love"
msgstr "لەم کاتەدا، خۆشەویستی بڵاوبکەرەوە"

#: src/gtd-empty-list-widget.c:63
msgid "Working hard is always rewarded"
msgstr ""

#: src/gtd-empty-list-widget.c:74
msgid "No tasks found"
msgstr "هیچ ئەرکێک نەدۆزرایەوە"

#: src/gtd-empty-list-widget.c:75
msgid "You can add tasks using the <b>+</b> above"
msgstr ""

#: src/gtd-plugin-dialog-row.c:97
msgid "Error loading extension"
msgstr "هەڵە لە بارکردنی پێوەکراو"

#: src/gtd-plugin-dialog-row.c:97
msgid "Error unloading extension"
msgstr ""

#: src/gtd-task-list-view.c:292
msgid "Removing this task will also remove its subtasks. Remove anyway?"
msgstr ""

#: src/gtd-task-list-view.c:295
msgid "Once removed, the tasks cannot be recovered."
msgstr ""

#: src/gtd-task-list-view.c:298 src/views/gtd-list-selector-panel.c:388
msgid "Cancel"
msgstr "پاشگەزبوونەوە"

#: src/gtd-task-list-view.c:300
msgid "Remove"
msgstr "سڕینەوە"

#: src/gtd-task-list-view.c:390
#, c-format
msgid "Done (%d)"
msgstr "تەواو (%d)"

#: src/gtd-task-list-view.c:660
#, c-format
msgid "Task <b>%s</b> removed"
msgstr ""

#: src/gtd-task-list-view.c:684
msgid "Undo"
msgstr "گەڕانەوە"

#: src/gtd-window.c:476
msgid "Details"
msgstr "وردەکارییەکان"

#: src/gtd-window.c:689
msgid "Loading your task lists…"
msgstr ""

#: src/gtd-window.c:793
msgid "Click a task list to select"
msgstr ""

#.
#. * If there's no task available, draw a "No tasks" string at
#. * the middle of the list thumbnail.
#.
#: src/views/gtd-list-selector-grid-item.c:250
msgid "No tasks"
msgstr "ئەرک نیە"

#: src/views/gtd-list-selector-panel.c:381
msgid "Remove the selected task lists?"
msgstr ""

#: src/views/gtd-list-selector-panel.c:384
msgid "Once removed, the task lists cannot be recovered."
msgstr "هەر کە سڕیتەوە، ئەوا لیستە ناتوانرێت بهێنرێتەوە."

#: src/views/gtd-list-selector-panel.c:396
msgid "Remove task lists"
msgstr "لیستی ئەرک دەسڕیتەوە"

#: src/views/gtd-list-selector-panel.c:709
msgid "Clear completed tasks"
msgstr "ئەرکە تەواوکراوەکان پاکبکەرەوە"
